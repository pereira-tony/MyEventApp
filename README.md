# EventApp

Application présentant les événements sur Paris avec la possibilité d'y participer.

## Technologie

React Native

## Dépendances utilisées

Gestionnaire de dépendances : npm

- @react-native-community/masked-view
- @react-navigation/native
- @react-navigation/stack
- Expo
- Moment
- Redux
- React-redux
- react-native-vector-icons (notamment pour FontAwesome)
- react-navigation
- react-navigation-stack
- react-navigation-tabs
- tcomb-form-native

## API utilisée

- Que faire à Paris (https://opendata.paris.fr/explore/dataset/que-faire-a-paris-/api/)