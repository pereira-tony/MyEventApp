import React, { Component } from "react";
import { YellowBox } from 'react-native'
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { persistStore, persistReducer } from 'redux-persist';
import {PersistGate } from 'redux-persist/integration/react';
import AsyncStorage from '@react-native-community/async-storage';
import Splash from "./screens/Splash";
import Home from "./screens/Home";
import Details from "./screens/Details";
import Profil from "./screens/Profil";
import Search from "./screens/Search";
import Login from './screens/Login';
import Icon from "react-native-vector-icons/FontAwesome";
import {Provider} from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './helpers/rootReducer'

YellowBox.ignoreWarnings([
	'VirtualizedLists should never be nested',
])

const persistConfig = {key: 'root', storage: AsyncStorage}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = createStore(persistedReducer);

const persistor = persistStore(store);



const BottomNavigator = createBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
        <Icon 
        name={"home"} 
        size={24} 
        color={tintColor} />
      )
  })
  },
  Search: {
    screen: Search,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
        <Icon 
        name={"search"} 
        size={24} 
        color={tintColor} />
      )
    })
  },
  Profil: {
    screen: Profil,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
        <Icon name={"user"} 
        size={24} 
        color={tintColor} />
      )
    })
  },
},
{
  tabBarOptions:{
    showLabel: false,
    activeTintColor: '#7766C6',
    inactiveTintColor: 'black',
  }
});
const AppNavigator = createStackNavigator(
  {
    Splash: { screen: Splash, navigationOptions: { headerShown: false } },
    Login:{screen: Login, navigationOptions: {headerShown: false}},
    Home: {
      screen: BottomNavigator,
      navigationOptions: { headerShown: false },
    },
    Details: { screen: Details, navigationOptions: { headerShown: false } },
  },
  {
    initialRouteName: "Splash",
  }
);

const AppContainer = createAppContainer(AppNavigator);


class App extends Component {
  render() {
    return <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
      <AppContainer />
      </PersistGate>
      </Provider>;
  }
}

export default App;
