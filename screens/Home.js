import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  FlatList,
} from "react-native";
import Title from "../components/Title";
import EventBox from "../components/EventBox";
import EventsService from "../services/events.service";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      eventsWeek: [],
      eventsAfter: [],
    };
  }

  async componentDidMount() {
    // let response = await EventsService.list();
    let eventsWeek = await EventsService.getEventsThisWeek(10);
    let eventsAfter = await EventsService.getEventsAfter();
    this.setState({ eventsWeek, eventsAfter });
  }

  render() {
    let { eventsWeek, eventsAfter } = this.state;

    return (
      <ScrollView style={styles.container}>
        <Title title={"Ce week-end"} />
        {/* {events.map((item) => {
          return <EventBox />;
        })} */}

        <FlatList
          data={eventsWeek}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          backgroundColor={"#FFF"}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <EventBox
              data={item.fields}
              horizontal={true}
              navigation={this.props.navigation}
            />
          )}
        />
        <Title title={"A venir"} />

        <FlatList
          data={eventsAfter}
          showsHorizontalScrollIndicator={false}
          backgroundColor={"#FFF"}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <EventBox data={item.fields} navigation={this.props.navigation} />
          )}
        />
      </ScrollView>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 70,
    backgroundColor: "#FFF",
  },
});
