import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Animated , Image} from 'react-native';
import logo from '../assets/logo.png';

class Splash extends Component{

  /**
   * Constructeur
   * @param {*} props 
   */
  constructor(props) {
    super(props);
    this.state = {
      fadeIn : new Animated.Value(0),
    }
  }

  fadeInAnimation = () => {
    let {fadeIn} = this.state
    Animated.timing(this.state.fadeIn, {
      toValue: 1,
      duration: 2000,
    }).start(() => this.props.navigation.navigate('Login'));
  }

  componentDidMount(){

    this.fadeInAnimation();
  }

  render() {
    console.log("Render");

    let { fadeIn } = this.state;
    return (
      <View style={styles.container}>
        
        <Animated.Image
          style={{opacity: fadeIn}}
          source={logo}
        />

      </View>
    )
  }
}

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF'
  }
});
