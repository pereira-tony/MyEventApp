import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TextInput, Button, TouchableOpacity
} from 'react-native';
import tcomb from 'tcomb-form-native';
import { connect } from 'react-redux';

//Initialisation
const Form = tcomb.form.Form;

// Model
const LoginModel = tcomb.struct({
    email: tcomb.String,
    password: tcomb.String
});

// Options
const options = {
    fields: {
        email: {
            label: "Mon email",
        }
    }
};

class Login extends Component{

    constructor(props) {
        super(props);

        this.containerRef = React.createRef();
        
        this.state = {
            email: null,
            password: null
        }
    }

    connect() {
        let valid = this.refs.form.validate();
    }

    handleChange(e, name) {
        console.log(e);
        this.setState({
            [name]: e.nativeEvent.text
        })
    }

    componentDidMount() {
       
    }

    render(){
        return (
            <View style={styles.container}>
               {/*  <TextInput 
                    // id={'email'}
                    onChange={(e) => this.handleChange(e, "email")}
                    style={styles.input}
                    placeholder={"Entrer votre email..."}
                />
                <TextInput 
                    id={'password'}
                    onChange={(e) => this.handleChange(e, "password")}
                    style={styles.input}
                    placeholder={"Entrer votre mot de passe..."}
                    secureTextEntry={true}
                /> */}

                {/* <TouchableOpacity style={styles.button}>
                    <Text>Se connecter</Text>
                </TouchableOpacity> */}

                <Form
                    ref="form"
                    type={LoginModel}
                    options={options}
                    value={this.state}
                />

                <Button title={"Se connecter"} onPress={() => this.connect()} />
                    
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    input: {
        width:"100%",
        fontSize:16,
        marginBottom:20,
        marginLeft:20
    }
})

export default Login;