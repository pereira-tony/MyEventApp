import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  StyleSheet,
  Button,
  Linking
} from "react-native";
import Title from "../components/Title";
import {updateFavoris} from '../actions/favoris.actions'
import {connect} from 'react-redux';

class Details extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: this.props.navigation.state.params.data,
      short_description: null,
      description: null,
      isFavoris: false,
    };
  }

  componentDidMount() {
    let { description } = this.state.data;
    let short_description =
      description.replace(/(<([^>]+)>)/ig, "").substr(0, 300) + "...";
    description = description.replace(/(<([^>]+)>)/ig, "");

    this.setState({ short_description, description });
    let isFavoris = false;
    let {favoris} = this.props;
    let {data} = this.state;
    favoris.includes(data.id) ? isFavoris = true : isFavoris = false;
    this.setState({short_description, description, isFavoris});
  }

  openDescription() {
    this.setState({ short_description: this.state.description });
  }

  gotToLink(){
      let {access_link} = this.state.data;
      Linking.canOpenURL(access_link)
  }

  addOrDeleteFavoris(){
    let {favoris} = this.props;
    let {id} = this.state.data;
    let {isFavoris} = this.state;

    favoris.includes(id) ? favoris.splice(favoris.indexOf(id), 1) : favoris.push(id);
    this.props.updateFavoris(favoris);
    this.setState({isFavoris: !!favoris.includes(id)})
  }


  /* addFavoris(){

    let {favoris} = this.props;
    let {id} = this.state.data;

    if(!favoris.includes(id)){
      favoris.push(id);
      this.props.updateFavoris(favoris);
      this.setState({isFavoris: true});
    };
  }

  deleteFavoris(){
    let {favoris} = this.props;
    let {id} = this.state.data;

    favoris.splice(favoris.indexOf(id), 1)
    this.props.updateFavoris(favoris);
    this.setState({isFavoris: false});
  } */


  render() {
    let {
      cover_url,
      title,
      contact_name,
      contact_phone
    } = this.state.data;

    let { short_description, isFavoris } = this.state;
    return (
      <ScrollView style={StyleSheet.container}>
        <ImageBackground
          style={styles.headerImage}
          source={{ uri: cover_url }}
        ></ImageBackground>
        <View style={styles.body}>
          <Title title={title} />
          <View>{/* Category */}</View>
          <View>{/* Info calendrier */}</View>
          <Text>A propos</Text>
          <Text onPress={() => this.openDescription()}>
            {short_description}
          </Text>

          <Text>Organisateur</Text>
          <Text>{contact_name}</Text>
          <Text>{contact_phone}</Text>


          <Button title={isFavoris ? "Supprimer des favoris" : "Ajouter aux favoris"} onPress={() => this.addOrDeleteFavoris()} />
{/* /* {

          isFavoris ? 
          <Button title={"Supprimer des favoris"} onPress={() => this.deleteFavoris()}/>
          :
          <Button title={"Ajouter aux favoris"} onPress={() => this.addFavoris()}/>
  } */ }

          <Button title={"Réservation"} onPress={() => this.gotToLink()} />
        </View>
      </ScrollView>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  headerImage: {
    height: 300,
  },
  body: {},
});


const mapStateToProps = state => {
  return {
    favoris: state.favoris
  }
}

const mapDispatchToProps = dispatch => {
  return{
    updateFavoris: favoris => {dispatch(updateFavoris(favoris))}
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Details);