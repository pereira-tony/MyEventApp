import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableHighlight,
} from "react-native";
import DateBox from "./DateBox";
import Helpers from "../helpers/Helpers";

class EventBox extends Component {
  details() {
    let { navigation, data } = this.props;

    // Navigation vers le détails de l'évènement
    navigation.navigate("Details", { data: data });
  }

  render() {
    let { title, cover_url, category, date_start } = this.props.data;
    let { horizontal } = this.props;
    return (
      <TouchableOpacity
        onPress={() => this.details()}
        navigation={this.props.navigation}
      >
        <View
          style={
            horizontal
              ? [styles.container, styles.containerHorizontal]
              : styles.container
          }
        >
          <Image
            style={styles.headerImage}
            source={{ uri: cover_url }}
            resizeMode={"cover"}
          />

          <View style={styles.body}>
            <DateBox
              style={styles.dateBox}
              day={Helpers.extractDayFromDate(date_start)}
              month={Helpers.extractMonthFromDate(date_start)}
            />
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.subtitle}>
              {Helpers.extractParentCategory(category)}
            </Text>
            <Text style={styles.subtitle}>{date_start}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default EventBox;

const styles = StyleSheet.create({
  container: {
    marginBottom: 45,
    marginHorizontal: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: "#FFF",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.08,
    shadowRadius: 20,
    elevation: 5,
  },

  containerHorizontal: {
    width: 300,
  },

  dateBox: {
    position: "absolute",
    left: 10,
    top: -45,
  },

  headerImage: {
    height: 120,
    width: "100%",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  body: {
    display: "flex",
    justifyContent: "center",
    height: 150,
    backgroundColor: "#fff",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: "600",
  },
  subtitle: {
    fontSize: 16,
    color: "#B0B0B0",
  },
});
